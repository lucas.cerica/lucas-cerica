#!/bin/bash

# Hago un clear de la pantalla
clear
# Generamos un case para las tareas que hay que hacer en base a las opcion del script que se ingrese
# Hago un if chico, si las opciones no estan entre 0 y 6 pido que ingresen un valor valido
if [ $@ -gt 7 ]; then
	echo "Ingreso una opcion invalida, por favor ejecute el script con un valor valido"
else
case $@ in
	0)
		echo "##################################"
		echo "### OPCION 0 - INSTALACION UFW ###"
		echo "##################################"
		echo ""
		read -p "Presione ENTER si quiere avanzar o Ctrl+C para cancelar"
		echo ""
		# Punto0 - Verificar instalacion de ufw
		# Verificar si esta instalado
		echo "#########################################"
		echo "### VERIFICANDO SI UFW ESTA INSTALADO ###"
		echo "#########################################"
		echo ""
		# Hago un which de ufw, si devuelve exit status 0 esta instaldo y guardo "Instalado" en variable UFW
		# Si devuelve otro valor que no sea cero, guardo en UFW "No instalado"
		# Esto lo usamos en el case debajo para instalar o no
		sudo which ufw >> /dev/null
		if [ $? == 0 ]; then
			UFW="Instalado"
		else
			UFW="NoInstalado"
		fi

		# Si UFW tiene guardado Instalado, indicamos que esta instalado correctamente
		# Si UFW tiene guardado NoInstalado, indicamos que se proceede con la instalacion, actualizamos base de datos e instalamos ufw
		case $UFW in
			Instalado)
				echo "ufw ya se encuentra instalado"
				;;
			NoInstalado)
				echo "ufw no esta instalado, aguarde mientras se procede a instalar, puede demorar unos instantes"
				sudo apt-get update >> /dev/null && sudo apt-get upgrade >> /dev/null
				sudo apt-get install -y ufw >> /dev/null
				echo "ufw instalado"
				;;
		esac
		;;
	1)
		# Cambiar politicas por defecto ufw
		echo "##############################################"
		echo "### OPCION 1 - CAMBIO DE POLITICAS DEFAULT ###"
		echo "##############################################"
		echo ""
		# Muestro el estado de las politicas default
		echo "El estado de las reglas default es la siguiente: "
		sudo ufw status verbose | grep Default
		echo ""
		read -p "Si desea modificar las reglas por default presione ENTER, o Ctrl+C para cancelar"
		# Pido el tipo de trafico a modificar y lo guardo en una variable
		echo -n "Ingrese el tipo de trafico que desea modificar (incoming/outgoing/ambos): "
		read TRAFFIC
		# Genero un caso, uno para incoming, otro outgoing, y si eligio ambos, pedir parametro para ambos
		case $TRAFFIC in
		        incoming)
		                echo -n "Desea permitir o denegar el trafico entrante? (allow/deny): "
		                read INCOMING
		                sudo ufw default $INCOMING incoming
		                echo "Los cambios fueron aplicados"
		                ;;
		        outgoing)
		                echo -n "Desea permitir o denegar el trafico saliente? (allow/deny): "
		                read OUTGOING
		                sudo ufw default $OUTGOING outgoing
		                echo "Los cambios fueron aplicados"
		                ;;
		        ambos)
		                echo -n "Desea permitir o denegar el trafico entrante? (allow/deny): "
		                read INCOMINGAMBOS
		                sudo ufw default $INCOMINGAMBOS incoming
		                echo -n "Desea permitir o denegar el trafico saliente? (allow/deny): "
		                read OUTGOINGAMBOS
		                sudo ufw default $OUTGOINGAMBOS outgoing
		                echo "Los cambios fueron aplicados"
		                ;;
		        *)
		                echo "Ingreso un valor que no corresponde"
		                ;;
		esac
		;;
	2)
		echo "####################################"
		echo "### OPCION 2 - ABM DE REGLAS UFW ###"
		echo "####################################"
		echo ""
		# Consulto si quiere realizar los cambios
		echo -n "Desea realizar ABM de reglas? (si/no): "
		read ABM
		# Mientras se elija que si, se avanza al menu de abm de reglas
		while [ $ABM == "si" ]
		do
		        echo -n "Indique la operacion que quiere realizar (alta/baja): "
		        read AB
		        case $AB in
				# Debido a que es una sintaxis especificando rango origen, segmento en si quiere de esa manera o no importa el rango origen
		                alta)
		                        echo -n "Desea especificar un rango origen para permitir o denegar? (si/no): "
		                        read ORIGEN
		                        if [ $ORIGEN == "si" ]; then
		                                echo -n "Indique rango o host de origen en formato CIDR (x.x.x.x/x): "
		                                read IPORIGEN
		                                echo -n "Indique operacion (allow/deny): "
		                                read ALLOWDENY
		                                echo -n "Indique numero de puerto: "
		                                read PUERTO
		                                echo -n "Indique protocolo (tcp/udp): "
		                                read PROTO
		                                sudo ufw $ALLOWDENY from $IPORIGEN to any port $PUERTO proto $PROTO 1>> /dev/null 2>> /tmp/errores.log
		                                echo "Cambios aplicados, mostramos tabla de reglas: "
		                                sudo ufw status numbered
		                        else
		                                echo -n "Indique operacion (allow/deny): "
		                                read ALLOWDENY
		                                echo -n "Indique servicio o puerto y protocolo (puerto/protocolo): "
		                                read PUERTO
		                                sudo ufw $ALLOWDENY $PUERTO 1>> /dev/null 2>> /tmp/errores.log
		                                echo "Cambios aplicados, mostramos tabla de reglas: "
		                                sudo ufw status numbered
		                        fi
					# Si elije si, sigue el bucle, sino, sale
		                        echo -n "Desea realizar otro cambio? (si/no): "
		                        read ABM
		                        ;;
				# Para baja de reglas muestro las reglas numeradas, que ponga el ID y eliminamos esa 
		                baja)
		                        echo "Estas son las reglas existentes: "
		                        sudo ufw status numbered
		                        echo -n "Indique el numero de regla que desea eliminar: "
		                        read DELETEID
		                        sudo ufw delete $DELETEID
		                        echo "Configuracion aplicada, las reglas quedaron de la siguiente manera: "
		                        sudo ufw status numbered
		                        echo -n "Desea realizar otro cambio? (si/no): "
		                        read ABM
		                        ;;
		                *)
		                        echo "Introdujo un valor invalido"
		                        ;;
		        esac
		done
		# En caso de que haya algun error mando los errores al fichero que indica el echo
		# por ejemplo, algun parametro mal ingresado
		echo "Si las reglas no se aplicaron correctamente y desea consultar mensajes de error, verificar el fichero /tmp/errores.log"
		echo "ABM de reglas finalizado"
		;;
	3)
		# Cambio de posicion ufw
		# Lo que hace este punto es pedir un ID y tomar datos de lo que hace la regla, con esos datos, la elimina y la
		# crea en la posicion del nuevo ID que se ingrese
		echo "###############################################"
		echo "### OPCION 3 - MOVER POSICION DE REGLAS UFW ###"
		echo "###############################################"
		# Muestro posiciones actuales
		echo -n "Desea modificar el orden de las reglas? (si/no): "
		read MODIFY
		
		while [ $MODIFY == "si" ]
		do
		       echo "Este es el estado numerado de reglas existentes: "
 		       sudo ufw status numbered
		       echo -n "La regla que desea modificar es permitida/denegada solo desde un host o rango origen? (si/no): "
		       read ORIGEN
		        if [ $ORIGEN == "si" ]; then
		                echo "Complete las siguientes opciones separadas por un espacio"
		                echo ""
		                echo -n "ID de regla a modificar orden, ID de nuevo orden, rango origen permitido (formato CIDR), allow/deny, puerto, tcp/udp: "
		                # Pido todos los parametros juntos separados por un espacio
				read IDMODIFY IDDESTINO RANGOORIGEN ALLOWDENY PUERTO PROTO
		                sudo ufw delete $IDMODIFY
		                sudo ufw insert $IDDESTINO $ALLOWDENY from $RANGOORIGEN to any port $PUERTO proto $PROTO
		                echo "Se hizo la modificacion, mostramos como queda el resultado: "
		                sudo ufw status numbered
		        else
		                echo "Ingrese los siguientes parametros de la regla a modificar separados por un espacio: "
		                echo -n "ID de regla a modificar, nuevo ID de regla, allow/deny, puerto, tcp/udp: "
		                read IDORIGEN IDDESTINO ALLOWDENY PUERTO PROTO
		                sudo ufw delete $IDORIGEN
		                sudo ufw insert $IDDESTINO $ALLOWDENY $PUERTO/$PROTO
		                echo "Se hizo la modificacion, mostramos resultado: "
		                sudo ufw status numbered
		        fi
		        echo -n "Desea realizar otra modificacion? (si/no): "
		        read MODIFY
		done
		# Muestro estado final
		echo "El estado final de las reglas es: "
		sudo ufw status numbered
		;;
	4)
		# Mostrar reglas
		echo "#################################"
		echo "### OPCION 4 - MOSTRAR REGLAS ###"
		echo "#################################"
		echo ""
		# Mostramos directamente las reglas con el comando
		echo "Las reglas de ufw actuales son: "
		sudo ufw status numbered
		read -p "Presione ENTER para finalizar"
		;;
	5)
		# Backup reglas
		# Dentro de /etc/ufw/ hay dos archivos que contiene las reglas que muestra
		# el comando ufw status: user.rules y user6.rules
		
		# Creamos directorios y ficheros para guardar backups
		mkdir -p $HOME/backups/
		BACKUPRULES4=$HOME/backups/user-rules4.$(date +%b.%Y).backup
		BACKUPRULES6=$HOME/backups/user-rules6.$(date +%b.%Y).backup
		
		echo "################################"
		echo "### OPCION 5 - BACKUP REGLAS ###"
		echo "################################"
		echo ""
		echo "Se procedera a generar dos archivos con el backup de las reglas solamente, tomadas de /etc/ufw/user.rules y /etc/ufw/user6.rules"
		echo "De forma preventiva tambien se hara un backup de todo el directorio /etc/ufw/"
		# Backup de /etc/ufw/user.rules solo tomaremos lo que dice el archivo entre RULES y END RULES y lo pasamos a un archivo
		# con !d al final del sed, pedimos que elimine del contenido del fichero, todo lo que no este enre RULES y END RULES
		sudo sed '/RULES/,/END RULES/!d' /etc/ufw/user.rules > $BACKUPRULES4
		# Hacemos lo mismo de /etc/ufw/user6.rules
		sudo sed '/RULES/,/END RULES/!d' /etc/ufw/user6.rules > $BACKUPRULES6
		# Tomamos una copia de todo el directorio /etc/ufw/
		sudo cp -R /etc/ufw/ $HOME/backups/backup-ufw/
		
		# Mostramos los resultados
		echo ""
		read -p "Presione ENTER para ver archivo de backup reglas IPv4: "
		cat $BACKUPRULES4
		echo ""
		read -p "Presione ENTER para ver archivo de backup reglas IPv6: "
		cat $BACKUPRULES6
		echo ""
		echo ""
		echo "Listamos el directorio donde se genero el backup de /etc/ufw/ almacenado en $HOME/backup-ufw/"
		read -p "Presione ENTER para ver el directorio listado"
		ls $HOME/backup-ufw/
		read -p "Consigna Finalizada, presione ENTER para salir"
		;;
	6)
		# Deshabilitar o habilitar ufw con el arranque del sistema
		# Lo que hace el script: si quiere que arranque con el sistema, ejecutamos systemctl enable ufw, si no quiere que arranque con el sistema
		# ejecutamos systemctl disable ufw
		echo "########################################################################"
		echo "### OPCION 6 - HABILITAR O DESHABILITAR UFW CON ARRANQUE DEL SISTEMA ###"
		echo "########################################################################"
		echo ""
		echo -n "Desea habilitar o deshabilitar ufw con el arranque del sistema? (enable/disable): "
		read UFWARRANQUE
		case $UFWARRANQUE in
		        enable)
		                sudo systemctl enable ufw >> /dev/null 2>&1
		                echo "ufw se iniciara con el arranque del sistema"
		                ;;
		        disable)
		                sudo systemctl disable ufw >> /dev/null 2>&1
		                echo "ufw no se iniciara con el arranque del sistema"
		                ;;
		        *)
		                echo $?
		                echo "Ingreso una opcion invalida, las correctas son 'enable' o 'disable'"
		                ;;
		esac
		;;
esac
fi
