# Declaramos el interprete
#!/bin/sh

# Creo el directorio /tmp/tareas/ a donde van a estar los archivos, si hay error enviar a /tmp/tarea1.err
mkdir /tmp/tareas/

# Declaro mis variables para el STOUT y STERR
REGOUT=/tmp/tareas/tarea1.txt
REGERR=/tmp/tareas/tarea1.err

## Contenido del script que va a /tmp/tareas/tarea1.txt
echo "Esta es mi primer tarea de programacion" | tee -a $REGOUT
echo "" | tee -a $REGOUT
echo "" | tee -a $REGOUT
echo "Creamos el directorio /tmp/tareas/ a donde se van a encontrar los archivos resultantes del script" | tee -a $REGOUT
echo "Los ficheros dentro de /tmp/tareas/ son tarea1.txt y tarea1.err"
echo "" | tee -a $REGOUT
echo "" | tee -a $REGOUT
echo "Este comando esta siendo ejecutado por $USER en el dia $(date +""%A" "%e" de "%b" del "%Y" a las "%T"")"| tee -a $REGOUT
echo "desde el directorio $PWD por el puerto $(tty)" | tee -a $REGOUT
echo "" | tee -a $REGOUT
echo "" | tee -a $REGOUT
echo "" | tee -a $REGOUT
echo "El login de $USER en la vm fue el $LOGINDATE" | tee -a $REGOUT

# Testeamos el funcionamiento del STERR que va a /tmp/tareas/tarea1.err
echo ""
echo ""
echo ""
echo "Consultamos el archivo /home/lcerica/noexiste para enviar la salida de error a /tmp/tareas/tarea1.err"
ls /home/lcerica/noexiste 2>>$REGERR | tee -a $REGOUT
