#!/bin/bash

# ---------------------------------------
# 		EXAMEN FINAL		-
# Fecha: 13/12/2021			-
# Nombre y Apellido: Lucas Cerica	-
# Materia: Programacion			-
# Profesor: Sergio Pernas		-
# ---------------------------------------

source ./funciones.sh

# Clear de pantalla
clear
# Valido que el parametro pasado el script sea valido y no vacio

if [[ -z $@ || $@ -ge 10 ]]; then
	echo "Ingreso un valor invalido, lea las instrucciones: "
	cat README.md
fi
	
case $@ in
	0)
		# Llamo a funcion
		# Bienvenida al final
		bienvenida
		;;
	1)
		# Llamo a funcion
		cat README.md
		;;
	2)
		# Ver como estaba hecho el script anterior
		echo "#####################################################"
		echo "### 	2 - REVISION SCRIPT ANTERIOR		###"
		echo "#####################################################"
		echo ""
		# Establezco YN en y para entrar al while y que siga ejecutando mientras sea "y"
		YN=y
		while [ $YN == "y" ]
		do
			# Muestro las opciones del fichero
			cat ./script-anterior/consignas.txt
			echo ""
			# Pido el punto para filtrar y mostrar solo ese punto, esta separado por ficheros
			echo -n "Ingrese la opcion que desea ver: "
			read PUNTO
			while [[ $PUNTO -eq 0 || $PUNTO -ge 7 ]]
			do	
				echo "Ingreso un valor invalido, los valores son entre 1 y 6"
				echo -n "Elija el valor: "
				read PUNTO
			done
			cat ./script-anterior/punto$PUNTO.txt
			echo ""
			echo -n "Desea consultar otro punto? (y/n): "
			read YN
			while [[ -z $YN || $YN != [yn] ]]
			do
				echo -n "Ingreso una opcion incorrecta, elija una opcion (y/n): "
				read YN
			done
			clear
		done
		;;
	3)
		echo "#############################################################"
		echo "### 		3 - CAMBIO DE PERMISOS			###"
		echo "#############################################################"
		echo ""
		# Llamo a funcion
		cambiopermisos	
		;;
	4)
		echo "#############################################################"
		echo "###		4 - CAMBIO OWNER Y GRUPO		###"
		echo "#############################################################"
		echo ""
		# Llamo a funcion
		cambioduenoygrupo
		;;
	5)
		echo "#############################################################"
		echo "###		5 - ALTA BAJA DE GRUPOS			###"
		echo "#############################################################"
		echo ""
		# Llamo a funcion
		abgroups
		;;
	6)
		echo "#############################################################"
		echo "###		6 - LISTADO DE GRUPOS			###"
		echo "#############################################################"
		echo ""
		# Listar grupos existentes, mostranso solo los nombres d elos grupos
		echo "Los grupos existentes son: "
		sudo cat /etc/group | cut -d : -f1 | more
		;;
	7)
		echo "#############################################################"
		echo "###		7 - BUSCADOR DE FICHEROS		###"
		echo "#############################################################"
		echo ""
		# Llamo a funcion
		buscarficheros
		;;
	8)
		# Definir exit status, si $? es 0, termino ok, sino, termino en error
		if [ $? == 0 ]; then
			echo "El programa finalizo correctamente"
		else
			echo "El programa finalizo en un estado de error"
		fi
		;;
esac
	
