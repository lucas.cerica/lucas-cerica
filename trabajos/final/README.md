
### EXAMEN FINAL

##### Alumno: Lucas Cerica
##### Materia: Programacion
##### Fecha: 13/12/2021
##### Profesor: Sergio Pernas

- El objetivo del script fue recrear la tarea 3, que en base a las correcciones que tuve durante la misma crei que habia
mucho por mejorar. A la misma se agregaron otros puntos que servian para darle alguna funcionalidad extra y usar conceptos 
en este nuevo script.

| **INSTRUCCIONES DE USO** | **Descripcion** | **Ejemplo** |
| --- | --- | --- |
| **Modo de ejecucion** | Ejecutar script y pasar una opcion | _./script.sh 0_ |
| **Opcion 0** | Bienvenida al Final | |
| **Opcion 1** | Leer Instrucciones | |
| **Opcion 2** | Consultar Programacion Script Anterior | _./script.sh 2_ |
| **Opcion 3** | Modificar permisos ficheros | |
| **Opcion 4** | Cambio Owner y Grupo de Ficheros | |
| **Opcion 5** | Crear y eliminar grupos | |
| **Opcion 6** | Listar Grupos Existentes | |
| **Opcion 7** | Buscador de Ficheros por Permisos | |
| **Opcion 8** | Exit Status del Programa | |

- {+ NOTAS +}
- El script debe ejecutarse como root o con usuarios que tengan permiso de sudo
- Las funciones se encuentran en el fichero funciones.sh

- [Este link te lleva al README.md del final](https://gitlab.com/lucas.cerica/lucas-cerica/-/blob/main/trabajos/final/README.md)
