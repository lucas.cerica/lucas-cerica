# Funcion bienvenida punto 1

bienvenida () {
	# Preparo directorio para mostrar en pantalla luego
	mkdir -p /tmp/figlet/
	FINALTEXT=/tmp/figlet/figlet.txt
	# Verifico si figlet esta instalado
	sudo which figlet >> /dev/null
	if [ $? == 0 ]; then
		FIGLET="Instalado"
	else
		FIGLET="NoInstalado"
	fi
	case $FIGLET in
		Instalado)
			# Si esta instalado lo ejecuto
			sudo figlet Final-Programacion1 > $FINALTEXT
			cat $FINALTEXT
			;;
		NoInstalado)
			# Si no esta instalado instalo y ejecuto
			sudo apt-get -y update >> /dev/null && sudo apt-get install -y figlet >> /dev/null
			sudo figlet Final-Programacion1 > $FINALTEXT
			cat $FINALTEXT
			;;
	esac
}

# Para usar en funciones donde tengo que validar si el fichero o directorio existen y no repetir lineas
existenciafichero () {
	read -e -p "Elija el fichero sobre el que desea realizar los cambios: " FICHERO
	echo ""
	# Testeamos si existe
	sudo test -e $FICHERO
	# Si $? es 1, no existe o hay algun error y lo mantengo en loop hasta que use uno correcto
	while [ $? == 1 ]
	do
		read -e -p "Ingreso un fichero inexistente, ingrese uno que exista: " FICHERO
		echo ""
	done
}

cambiopermisos () {
	# Con YN en "y" entro al loop
	YN=y
	while [ $YN == "y" ] 
	do
		# Con la funcion pido un fichero, testeo existencia y muestro los permisos actuales
		existenciafichero
		echo "Los permisos actuales del fichero ingresado son: "; ls -l $FICHERO | cut -f1 -d ' '
		echo ""
		echo -n "Desea cambiar permisos de usuario, grupo u otros?: (u/g/o): "
		read UGO
		while [[ -z $UGO || $UGO != [ugo] ]]
		do
			echo -n "Ingreso una opcion incorrecta, ingrese la opcion nuevamente (u/g/o): "
			read UGO
		done
		echo ""
		# Muestro opciones y en base a la que elija ejecuto el chmod con los parametros para cumplir
		echo "OPCIONES DE PERMISOS"
		echo ""
		echo "0 - Ninguno"
		echo "1 - Ejecucion"
		echo "2 - Escritura"
		echo "3 - Escritura y Ejecucion"
		echo "4 - Lectura"
		echo "5 - Lectura y Ejecucion"
		echo "6 - Lectura y Escritura"
		echo "7 - Lectura, Escritura y Ejecucion"
		echo ""
		echo -n "Ingrese la opcion deseada: "
		read PERMISOS
		# Valido el valor que sea correcto
		while [[ -z $PERMISOS || $PERMISOS -ge 8 ]]
		do
			echo -n "Ingreso una opcion incorrecta, elija un valor de 0 a 7: "
			read PERMISOS
		done
		case $PERMISOS in
			0)
				sudo chmod $UGO-r-w-x $FICHERO
				;;
			1)
				sudo chmod $UGO-r-w+x $FICHERO
				;;
			2)
				sudo chmod $UGO-r+w-x $FICHERO
				;;
			3)
				sudo chmod $UGO-r+w+x $FICHERO
				;;
			4)
				sudo chmod $UGO+r-w-x $FICHERO
				;;
			5)
				sudo chmod $UGO+r-w+x $FICHERO
				;;
			6)
				sudo chmod $UGO+r+w-x $FICHERO
				;;
			7)
				sudo chmod $UGO+r+w+x $FICHERO
				;;
		esac
		echo ""
		# Muestro los permisos, pregunto si quiere seguir o terminar
		echo "Los permisos quedaron de la siguiente manera: "; ls -l $FICHERO | cut -d ' ' -f1 
		echo ""
		echo -n "Desea continuar realizando cambios? (y/n): "
		read YN
		while [[ -z $YN || $YN != [yn] ]]
		do
			echo -n "Ingreso una opcion no valida, ingrese un valor correcto (y/n): "
			read YN
		done
	done
}

cambioduenoygrupo () {
	# Con YN en "y" entro al while
	YN=y
	while [ $YN == "y" ]
	do
		# Pido fichero y testeo que exista con la funcion
		existenciafichero
		# Muestro valores actuales y pido el nuevo owner
		echo "El owner y grupo de $FICHERO son: "; ls -l $FICHERO | cut -f3,4 -d ' '
		echo ""
		echo -n "Ingrese el nuevo propietario del fichero: "
		read NEWOWNER
		# Testeo la existencia del usuario
		sudo grep $NEWOWNER /etc/passwd >> /dev/null  
		while [[ -z $NEWOWNER || $? == 1 ]]
		do
			echo -n "Ingreso un valor nulo o el usuario no existe, ingrese uno correcto: "
			read NEWONER
		done
		# Pido el nuevo grupo opcionalmente
		echo -n "Ingrese el nuevo grupo (opcional): "
		read NEWGROUP
		echo -n "Desea aplicar el cambio a subcarpetas y ficheros? (y/n): "
		read SINO
		case $SINO in
			y)
				# Aplicamos cambios a subcarpetas y archivos
				sudo chown -R $NEWOWNER:$NEWGROUP $FICHERO
				;;
			n)
				# Aplicamos el cambio sin afectar subcarpetas y archivos
				sudo chown $NEWOWNER:$NEWGROUP $FICHERO
				;;
		esac
		echo ""
		# Muestro estado final
		echo "El fichero $FICHERO quedo con el siguiente owner y grupo: "; ls -l $FICHERO | cut -f3,4 -d ' '
		echo ""
		echo -n "Desea continuar aplicando cambios? (y/n): "
		read YN
		while [[ -z $YN || $YN != [yn] ]]
		do
			echo -n "Ingreso una opcion no valida, ingrese un valor correcto (y/n): "
			read YN
		done	
	done
}

abgroups () {
	YN=y
	while [ $YN == "y" ]
	do
		# Pido si quiere crear, valido la entrada y segun lo que elija ejecuto el comando de add o del
		echo -n "Desea crear o eliminar un grupo? (crear/eliminar): "
		read ABGROUP
		while [[ -z $ABGROUP || $ABGROUP != "crear" && $ABGROUP != "eliminar" ]]
		do
			echo -n "Eligio una opcion incorrecta, escriba nuevamente (crear/eliminar): "
			read ABGROUP
		done
		case $ABGROUP in
			crear)
				echo -n "Ingrese el nombre del grupo a crear: "
				read NEWGRP
				while [ -z $NEWGRP ]
				do
					echo -n "Por favor ingrese un nombre: "
					read NEWGRP
				done
				sudo groupadd $NEWGRP
				echo "Se creo el grupo $NEWGRP"
				echo "Filtramos la lectura del fichero /etc/group"; sudo cat /etc/group | grep $NEWGRP
				;;
			eliminar)
				echo -n "Ingrese el nombre del grupo a eliminar: "
				read DELGRP
				while [ -z $DELGRP ]
				do
					echo -n "Por favor ingrese un grupo: "
					read DELGRP
				done
				sudo groupdel $DELGRP
				echo "Se elimino el grupo $DELGRP"
				;;
		esac
		echo -n "Desea continuar haciendo modificaciones? (y/n): "
		read YN
		while [[ -z $YN || $YN != [yn] ]]
		do
			echo -n "Ingreso una opcion no valida, indique un valor correcto (y/n): "
			read YN
		done

	done
}

buscarficheros () {
	YN=y
	while [ $YN == "y" ]
	do
		# Muestro opciones, pido el path de busqueda, lo valido y en base a la opcion ejecuto el find
		echo "OPCIONES DE BUSQUEDA"
		echo ""
		echo "0 - Ninguno"
		echo "1 - Ejecucion"
		echo "2 - Escritura"
		echo "3 - Escritura y Ejecucion"
		echo "4 - Lectura"
		echo "5 - Lectura y Ejecucion"
		echo "6 - Lectura y Escritura"
		echo "7 - Lectura, Escritura y Ejecucion"
		echo ""
		read -e -p "Ingrese el path del directorio en el cual se realizara la busqueda (/path/del/directorio): " path
		echo ""
		while [ ! -d "$path" ]
		do
			read -e -p "Ingreso un path inexistente, ingrese nuevamente: " path
		done
		echo -n "Ingrese la opcion de permisos que quiere buscar (0 a 7): "
		read OPCION
		while [[ -z $OPCION || $OPCION -ge 8 ]]
		do
			echo -n "Ingreso un valor erroneo, vuelva a ingresar (0 a 7): "
			read OPCION
		done
		case $OPCION in
			0)
				echo "Los ficheros son: "
			       	sudo find $path -perm /000
				;;
			1)
				echo "Los ficheros son: " 
				sudo find $path -perm /001
				;;
			2)
				echo "Los ficheros son: "
				sudo find $path -perm /010
				;;
			3)
				echo "Los ficheros son: "
				sudo find $path -perm /011
				;;
			4)
				echo "Los ficheros son: "
				sudo find $path -perm /100
				;;
			5)
				echo "Los ficheros son: "
				sudo find $path -perm /101
				;;
			6)
				echo "Los ficheros son: "
				sudo find $path -perm /110
				;;
			7)
				echo "Los ficheros son: "
				sudo find $path -perm /111
				;;
		esac
		echo -n "Desea realizar otra busqueda? (y/n): "
		read YN
		while [[ -z $YN || $YN != [yn] ]]
		do
			echo -n "Por favor ingrese un valor valido (y/n): "
			read YN
		done
	done
}

