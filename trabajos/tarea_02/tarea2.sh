#!/bin/bash
# Declarado el interprete de comandos

# Tarea 02 - Programacion 1
# Lucas Cerica

# P1 - Crear una copia de backup del directorio de usuario de cada usuario del sistema

# Creamos el directorios a donde se guardan los backups y los logs
mkdir -p $HOME/backups/
mkdir -p /tmp/$USER/

# Elegimos el directorio para las copias y lo registramos en una variable
# Declaro variable con el fichero de backup
BACKUPHOME=$HOME/backups/$USER.$(date +%b.%e.%Y.%H.%M.%S).tar.gz
BACKUPLOG=/tmp/$USER/backup.log
BACKUPERR=/tmp/$USER/backup.err

# Declaro 2 variables para errores o informe de todo lo resguardado
LOGSCRIPT=/tmp/$USER/script.log
LOGSCRIPTERR=/tmp/$USER/script.err

# Variable que indica la hora de ejecucion del comando
TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`

# Limpiamos la pantalla para comenzar y mostramos un cargando...
clear
echo "Cargando primer punto"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s

# Indicamos tarea a realizar, los directorios y el usuario
echo "Se realizara un respaldo del directorio '$HOME' en '$HOME/backups' del usuario $USER"
sleep 1s

# Tomamos la hora de inicio del backup lo enviamos a las variables de backup
echo "La hora de inicio del backup es $(date +%T)" 2>>$BACKUPERR | tee -a $BACKUPLOG
echo "" | tee -a $BACKUPLOG
sleep 1s
# Ejecutamos el backup, enviamos el detalle a BACKUPLOG, errores a BACKUPERR y TIMESTAMP a LOGSCRIPT, errores del tar a LOGSCRIPTERR
tar cvzf $BACKUPHOME --absolute-names $HOME 1>>$BACKUPLOG 2>>$BACKUPERR
echo "$TIMESTAMP: Ejecucion tar" 1>>$LOGSCRIPT 2>>$LOGSCRIPTERR

# Mostramos en la pantalla de ejecucion que se esta ejecutando
echo "Ejecutando backup" | tee -a $BACKUPLOG
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
sleep 1s
echo -ne " Finalizado\r"
echo -ne "\n"
echo ""

# Tomamos la hora de fin y la enviamos a las variables, tambien la mostramos en pantalla
echo "La hora de fin es $(date +%T)" 2>>$BACKUPERR | tee -a $BACKUPLOG
echo "" | tee -a $BACKUPLOG
sleep 1s

# Indicamos que finalizo la operacion e imprimimos una nota de los directorios donde esta la informacion
echo "Operacion Finalizada" | tee -a $BACKUPLOG
sleep 1s
echo "" | tee -a $BACKUPLOG
echo "NOTA: Se guardo el archivo $BACKUPHOME, Logs del backup en $BACKUPLOG y errores en $BACKUPERR" | tee -a $BACKUPLOG
echo "Log del script en $LOGSCRIPT y errores en $LOGSCRIPTERR" | tee -a $BACKUPLOG
sleep 5s

# Mostramos un mensaje para detener y que se pueda ver el mensaje en pantalla y apretar ENTER para ir al punto siguiente
read -p "Presione ENTER para avanzar al Segundo Punto o CTRL C para finalizar" && \

# SEGUNDO PUNTO
# Fichero comun con timestamp de inicio de sesion de cada usuario del sistema
# Almacenar en un fichero por usuario el tiempo de inicio de sesion
# Crear una variable de entorno accesible para todos los usuarios con el timestamp de inicio de sesion

# Creo directorios por si no existen
mkdir -p /tmp/userslogin/
mkdir -p /tmp/$USER/
# Declaro variable con fichero comun donde va a esta el timestamp del log de los usuarios
LOGTSTAMPGLOBAL=/tmp/userslogin/login.log

# Declaro variable con fichero donde va a estar el timestamp del log por usuario
LOGTSTAMP=/tmp/$USER/login.log

# Variable por cualquier error en el script
ERRLOG=/tmp/$USER/script2.err

# Creo los ficheros y les doy permisos para que lo puedan escribir otros usuarios
chmod o+w /tmp/userslogin
touch /tmp/userslogin/login.log
chmod o+w /tmp/userslogin/login.log

# Cleareo la pantalla del primer punto y muestro un scargando del segundo punto
clear
echo "Cargando segundo punto"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s
echo ""

# Mostramos mensaje para pedir clave de root y poder exportar la variable a /etc/profile
echo "Para exportar variable general ingrese clave de root o aguarde si es root"
sleep 1s
# Exporto variable general DATELOGIN para tomar el login de todos los usuarios
su - -c "echo export DATELOGIN=\$\(date\) >> /etc/profile" root

# Mando marca de tiempo del export a las variables de log del script
echo "$TIMESTAMP: Export Variable General DATELOGIN a /etc/profile" 1>>$LOGSCRIPT 2>>$LOGSCRIPTERR
echo ""
# Muestro mensaje de lo que se esta haciendo en pantalla
echo "Exportando variable a /etc/profile"

sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s
echo ""

# Hacemos que si hay algun error se envie el mismo al fichero /tmp/$USER/script2.err
cat /etc/profile | grep DATELOGIN 1>>/dev/null 2>>$ERRLOG

# Imprimo los login de los usuarios para enviar a los ficheros
echo "$DATELOGIN: Se logueo el usuario $USER" 1>>$LOGTSTAMPGLOBAL 2>>$ERRLOG
echo "$DATELOGIN: Se logueo el usuario $USER" 1>>$LOGTSTAMP 2>>$ERRLOG

# Finaliza el punto dos, lo muestro en pantalla e indico como funciona
echo "Punto 2 finalizado, a partir del proximo login de usuario y se ejecute el script, se guardara su timestamp de login"
echo "dentro de /tmp/$USER/login.log y en el archivo general /tmp/userslogin/login.log"

# Dejo mensaje en pantalla para poder leer todo y apretar un enter para avanzar al ultimo punto
read -p "Presione ENTER para avanar al Tercer Punto o CTRL C para finalizar" && \

# TERCER PUNTO - Cree un script que
# Tenga distintos tipos de variables con valores obtenidos como resultados de comandos
# Llame a la ejecucion de un segundo script
# El segundo script debe realizar alguna operacion o tarea con las variables del primer script

# Creo dos directorios con archivos
mkdir -p /tmp/figlet/
mkdir -p /tmp/figlet2/

# Creo dos archivos para almacenar informacion
touch /tmp/figlet/figlet.txt
touch /tmp/figlet2/figlet2.txt

# Declaro variables que se usan en este script y va a utilizar el segundo
FIGLETTEXT=/tmp/figlet/figlet.txt
LOGFIGLET=/tmp/figlet2/figlet2.txt
LOGFIGLETERR=/tmp/figlet2/figlet2.err
TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`

# Las exporto para que esten disponibles en la sesion actual
export FIGLETTEXT=/tmp/figlet/figlet.txt
export LOGFIGLET=/tmp/figlet2/figlet2.txt
export LOGFIGLETERR=/tmp/figlet2/figlet2.err
export TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
sleep 1s

# Buscamos si esta instalado figlet, si no esta, lo instalamos y voy mostrando mensajes en pantalla
clear
echo "Instalaremos el paquete figlet si no esta instalado"
echo ""
sleep 1s
echo "Buscando paquete figlet"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s
which figlet > /dev/null || echo "Paquete no encontrado, inserte clave root para instalar" &&
        which figlet > /dev/null ||
        su - -c "apt update 2>/dev/null && apt install -y figlet || yum install -y figlet" root &&
        echo "Paquete instalado"
sleep 3s
# Ejecuto figlet y mando mensajes a las variables 
figlet Punto3-Programacion > /tmp/figlet/figlet.txt && echo "$TIMESTAMP: Ejecucion figlet" 1>>$LOGFIGLET 2>>$LOGFIGLETERR

# Mensaje para que se muestre en pantalla que se va a ejecutar el segundo script
echo "Llamando a segundo script"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s

# Ejecuto el segundo script, lo que hace es hacer un CAT de las variables LOGFIGLET y FIGLETTEXT declaradas en este script
bash tarea2-script2.sh

# Muestro mensaje de final del script y enter para terminar completamente
echo ""
echo "Final del Script"
read -p "Presione ENTER o CTRL C para finalizar"
