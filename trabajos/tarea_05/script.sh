#!/bin/bash
source ./funciones.sh
# Cleareo la pantalla
clear

# Valido que el parametro ingresado al script sea valido y no vacio
if [[ -z $@ || $@ -ge 8 ]]; then
	echo "Ingreso un valor invalido, este es el instructivo: "
	cat instrucciones.txt
fi

case $@ in
	1)
		echo "######################################"
		echo "### PUNTO 1 - CAMBIO HOSTNAME E IP ###"
		echo "######################################"
		echo ""
		# Preguntamos para saber si vamos a parte de Hostname o IP
		echo -n "Desea realizar cambio de hostname o IP? (hostname/ip): "
		read HOSTIP
		# Valido que el valor sea correcto
		while [[ $HOSTIP != "hostname" && $HOSTIP != "ip" ]]
		do
			echo -n "Por favor ingrese un parametro valido (hostname/ip): "
			read HOSTIP
		done
		
		case $HOSTIP in
			hostname)
				# Si quiere cambiar, ejecuto hostnamectl y pregunto si reiniciamos el equipo
				echo -n "Desea realizar cambio de hostname? (y/n): "
				read YN1
				while [ $YN1 == "y" ]
				do
					echo -n "Ingrese el nuevo hostname: "
					read HOSTNAME
					hostnamectl set-hostname $HOSTNAME
					echo ""
					echo -n "NOTA: Debe reiniciar el equipo para hacer el cambio efectivo, desea reiniciar? (y/n): "
					read INIT
					if [ $INIT == "y" ]; then
						sudo init 6
					else
						# Si no quiere reiniciar, pregunto si quiere otro cambio o continuar
						echo ""
						echo -n "Desea realizar otro cambio? (y/n): "
						read YN1
					fi
				done
				;;
			ip)
				echo "IMPORTANTE: Usted selecciono cambio de IP, recomendamos ejecutar el script por consola en este caso, ya que si cambia la IP perdera la conexion remota"
				echo -n "Desea cambiar de IP? (y/n): "
				read YN2
				
				while [ $YN2 == "y" ]
				do
					# Llamo a la funcion cambioip y cuando termina, pregunto si quiere seguir haciendo cambios o no
					cambioip
					echo ""
					echo -n "Desea realizar otra modificacion? (y/n): "
					read YN2
				done
				;;
		esac
		;;
	2)
		echo "###################################################################"
		echo "### PUNTO 2 - INSTALACION/DESINSTALACION COMPONENTES STACK LAMP ###"
		echo "###################################################################"
		echo ""
		echo -n "Desea avanzar con el punto 2? (y/n): "
		read YN
		while [ $YN == "y" ]
		do
			# Muestro el fichero con las opciones
			cat instrucciones-punto2.txt
			echo ""
			echo -n "Que opcion desea?: "
			read OPCION
			clear
			while [[ -z "$OPCION" || $OPCION -ge 7 ]]
			do
			        echo "Ingreso un valor invalido, estas son las opciones: "
				echo ""
			        cat instrucciones-punto2.txt
				echo ""
				echo -n "Elija la opcion: "
				read OPCION
			done
			case $OPCION in
				# En base a la opcion, llamo a una funcion especifica
				1)
					isoloapache
					;;
				2)
					iapachephp
				;;
				3)
					iapachephpmysql
					;;
				4)
					dsoloapache
					;;
				5)
					dapachephp
					;;
				6)
					dapachephpmysql
					;;
			esac
			echo -n "Desea continuar modificando? (y/n): "
			read YN
		done
		;;
	3)
		echo "######################################"
		echo "### PUNTO 3 - ALTA/BAJA DE SITIOS  ###"
		echo "######################################"
		echo ""
		echo -n "Desea avanzar con las altas y baja de sitios? (y/n): "
		read YN
		while [ $YN == "y" ]
		do
			echo -n "Desea realizar alta o baja de un sitio? (a/b): "
			read AB
	                while [[ -z "$AB" || $AB != [ab] ]]
	                do
	                        echo "Ingreso un valor invalido, estas son las opciones (a/b)"
	                        echo ""
				echo -n "Elija la opcion (a/b): "
	                        read AB
	                done
	                case $AB in
				a)
					altasitio
					;;
				b)
					bajasitio
					;;
			esac
			echo -n "Desea continuar haciendo cambios? (y/n): "
			read YN
		done
		;;
	4)
		echo "##############################################"
		echo "### 	  PUNTO 4 - ESTADISTICAS 	 ###"
		echo "##############################################"
		echo ""
		echo -n "Desea avanzar con la consulta de estadisticas? (y/n): "
		read YN
                while [ $YN == "y" ]
                do
                        # Muestro el fichero con las opciones
                        cat instrucciones-punto4.txt
                        echo ""
                        echo -n "Que opcion desea?: "
                        read OPCION
                        clear
                        while [[ -z "$OPCION" || $OPCION -ge 3 ]]
                        do
                                echo "Ingreso un valor invalido, estas son las opciones: "
                                echo ""
                                cat instrucciones-punto4.txt
                                echo ""
                                echo -n "Elija la opcion: "
                                read OPCION
                        done
                        case $OPCION in
				1)
					echo -n "Desea consultar la cantidad de visitas que tuvo un sitio? (y/n): "
					read YN
					while [[ -z "YN" || $YN != [yn] ]]
					do
						echo "Ingreso un valor invalido, estas son las opciones (y/n)"
						echo ""
						echo -n "Elija la opcion (y/n): "
						read YN
					done
					while [ $YN == "y" ]
					do
						contarvisitas
						echo ""
						echo -n "Desea realizar otra consulta? (y/n): "
						read YN
					done
					;;
				2)
					echo -n "Desea consultar la IP y hora de visitas al sitio? (y/n): "
					read YN
					while [[ -z "YN" || $YN != [yn] ]]
					do
						echo "Ingreso un valor invalido, las opciones son: (y/n)"
						echo
						echo -n "Elija la opcion (y/n): "
						read YN
					done
					while [ $YN == "y" ]
					do
						iptimestamp
						echo ""
						echo -n "Desea realizar otra consulta? (y/n): "
						read YN
					done
					;;
			esac
		done
		;;
	5)
		echo "###############################################"
		echo "### 	PUNTO 5 - DEPLOY WORDPRESS 	  ###"
		echo "###############################################"
		echo ""
		echo "#####################################################################"
		echo "### IMPORTANTE: Para avanzar, debe tener instalado el stack LAMP  ###"
		echo "#####################################################################"
		echo ""
		echo "Antes de comenzar con del deploy de Wordpress verificaremos que tenga el stack LAMP instalado y en caso de no estarlo, se instalara"
		read -p "Presione ENTER para avanzar o Ctrl+C para cancelar"
		
		# Llamamos a funcion de instalacion stack LAMP
		iapachephpmysql
		echo ""
		echo "Se comenzara con el deploy de Wordpress"
		# Llamamos a la funcion
		deploywordpress
		;;
	6)
		echo "######################################"
		echo "###    PUNTO 6 - BACKUP APACHE     ###"
		echo "######################################"
		echo ""
		echo -n "Desea realizar backup de toda la configuracion de apache? (y/n): "
		read YN
		while [ $YN == "y" ]
		do
			echo -n "Ingrese la direccion del servidor destino: "
			read IPDESTINO
			# Creamos directorio de backup en destino
			echo "Vamos a crear el directorio de backup en el servidor destino"
			ssh root@$IPDESTINO 'mkdir -p /root/backup-apache/'
			# Enviamos ficheros de backup a destino
			echo "Vamos a enviar los ficheros al directorio /root/backup-apache/"
			sudo scp -r /etc/apache2/ /var/www/ /var/log/apache2/ root@$IPDESTINO:/root/backup-apache/
			echo ""
			echo "Se realizo el backup al servidor destino en el directorio /root/backup-apache/"
			echo -n "Desea realizar otro backup? (y/n): "
			read YN
		done
		;;
	7)
		echo "######################################"
		echo "###    PUNTO 7 - MIGRAR SITIOS	 ###"
		echo "######################################"
		echo ""
		echo "REQUISITOS: 1- Tener instalado apache2 en servidor destino"
		echo "	    2- Tener habilitado Root Login por SSH al servidor destino"
		echo ""
		echo -n "Cumplis los requisitos? (y/n): "
		read YN
		if [ $YN == "y" ]; then
			echo -n "Indique el sitio que desea migrar: "
			read SITIO
			echo -n "Indique IP del servidor origen: "
			read IPORIGEN
			echo -n "Indique IP del servidor destino: "
			read IPDESTINO
			# Enviar el fichero de config
			echo ""
			echo "Migraremos el directorio /etc/apache2/sites-available/$SITIO.conf"
			sudo scp /etc/apache2/sites-available/$SITIO.conf root@$IPDESTINO:/etc/apache2/sites-available/
			# Enviar directorio con ficheros
			echo ""
			echo "Migraremos el directorio /var/www/$SITIO/"
			sudo scp -r /var/www/$SITIO/ root@$IPDESTINO:/var/www/
			# Activar sitio en destino
			echo "Activar sitio"
			ssh root@$IPDESTINO "a2ensite $SITIO"
			echo ""
			echo "Reiniciar apache en Servidor remoto"
			ssh root@$IPDESTINO 'systemctl reload apache2'
			# Actualizar Fichero Hosts
			echo ""
			sudo sed -i "s/$IPORIGEN/$IPDESTINO/g" /etc/hosts
			echo "Se actualizo el fichero /etc/hosts con la nueva IP"
		else
			echo "Instale apache2 y permita acceso ssh a root en el servidor destino y vuelva a ejecutar"
		fi
		;;
esac
