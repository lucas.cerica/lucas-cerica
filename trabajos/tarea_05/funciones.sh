# Funcion Cambio de IP

cambioip () {
        # Mostramos config del fichero de interfaces
	echo "Esta es su configuracion actual: "
	sudo cat /etc/network/interfaces
	# Consulto ya que si tiene DHCP modificamos el fichero de una manera y si tiene estatica de otra
	echo -n "Actualmente usa DHCP o IP Estatica? (dhcp/static): "
        read TYPE
	# Validamos que ingrese una opcion valida
	while [[ $TYPE != "dhcp" && $TYPE != "static" ]]
	do
		echo -n "Por favor ingrese un valor valido (dhcp/static): "
		read TYPE
	done
		case $TYPE in
			# Si es estatica solo cambiamos la IP en el fichero y listo
	            	static)
				echo -n "Ingrese el nombre de la interfaz: "
				read INTERFACE
	                        echo -n "Ingrese la ip actual: "
	                        read OLDIP
	                        echo -n "Ingrese la nueva ip: "
	                        read NEWIP
	                        sudo sed -i "s/$OLDIP/$NEWIP/g" /etc/network/interfaces
	                        ;;
			# Si tiene DHCP cambiamos por estatica y agregamos las lineas address, netmask y gateway
	                dhcp)
	                        echo "Se modificara configuracion DHCP por Estatica, si esta de acuerdo"
	                        read -p "presione ENTER para continuar o Ctrl+C para cancelar"
				echo -n "Ingrese nombre de interfaz a modificar: "
				read INTERFACE
 	                        echo -n "Ingrese nueva IP: "
	                        read NEWIP
	                        echo -n "Ingrese mascara: "
	                        read MASK
	                        echo -n "Ingrese gateway: "
	                        read GW
	                        sudo sed -i "s/iface $INTERFACE inet dhcp/iface $INTERFACE inet static\naddress $NEWIP\nnetmask $MASK\ngateway $GW/g" /etc/network/interfaces
	        esac
		# Mostramos como queda el fichero
		echo ""
		echo ""
	        echo "La configuracion del fichero quedo de la siguiente manera: "
	        cat /etc/network/interfaces
		sleep 2s
		echo ""
		sleep 1s
		echo "Vamos a Reiniciar el Servicio de Red"
		sudo systemctl restart networking
		#ip -4 addr show $INTERFACE
		sleep 1s
	#	echo "segundo ifup"
		sudo ifup $INTERFACE
		echo "La IP en la interfaz $INTERFACE quedo de la siguiente manera: "
		ip -4 addr show $INTERFACE
		# Lo pausamos con un read para que verifique y luego del ENTER continue
		echo ""
		read -p "Presione ENTER para avanzar"
}

# Funcion Instalacion Solo Apache

isoloapache () {
	# Verifico que apache no este instalado
	echo "Vamos a Verificar si Apache se encuentra instalado, para eso necesitamos su IP"
	echo -n "Por favor ingrese su IP: "
       	read IP
	wget -o /dev/null http://$IP
	if [ $? == 0 ]; then
		APACHE="Instalado"
	else
		APACHE="NoInstalado"
	fi
	case $APACHE in
		Instalado)
			echo "Apache ya se encuentra instalado"
			;;
		NoInstalado)
			echo "Apache no esta instalado, aguarde mientras se procede a instalar, puede demorar unos instantes"
			sudo apt-get -y update >> /dev/null && sudo apt-get install -y apache2 >> /dev/null
			echo ""
			# Inicio el servicio
			sudo systemctl start apache2
			echo "Apache Instalado"
			;;
	esac
}

# Funcion Instalacion Apache y PHP
iapachephp () {
	# Verifico que no esten instalados, si no estan los instalo
	isoloapache
	# Verifico la instalacion de php
	echo ""
	echo "Vamos a verificar si PHP ya esta instalado"
	echo ""
	php -v > /dev/null 2>&1
	if [ $? == 0 ]; then
		PHP="Instalado"
	else
		PHP="NoInstalado"
	fi
	case $PHP in
		Instalado)
			echo "PHP ya se encuentra instalado, la version es: "
			php -v | head -1
			;;
		NoInstalado)
			echo ""
			echo "No se encontro PHP instalado, se instalara con sus librerias, por favor aguarde"
			echo ""
			sudo apt-get -y update >> /dev/null && sudo apt-get install -y php libapache2-mod-php >> /dev/null
			echo "Se instalo la siguiente version de php: "
			echo ""
			php -v | head -1
			# Reiniciamos Apache
			sudo systemctl restart apache2
			;;
	esac
}

iapachephpmysql () {
	# Verificamos instalacion de apache y php
	iapachephp
	# Verificar instalacion mysql
	echo ""
	echo "Vamos a verificar si MySQL esta instalado"
	echo ""
	sudo mysqld --version > /dev/null 2>&1
	if [ $? == 0 ]; then
		MYSQL="Instalado"
	else
		MYSQL="NoInstalado"
	fi
	case $MYSQL in
		Instalado)
			echo "MySQL ya se encuentra instalado, la version es: "
			echo ""
			sudo mysqld --version
			;;
		NoInstalado)
			echo "No se encontro MySQL instalado, se procedera a instalar, por favor aguarde"
			echo ""
			sudo apt-get -y update >> /dev/null & sudo apt-get install -y default-mysql-server >> /dev/null
			echo "Se instalo la siguiente version de MySQL: "
			sudo mysqld --version
			# Reiniciamos el servicio
			sudo systemctl restart apache2
			sudo systemctl start mysqld
			;;
	esac
	# Generamos un fichero para poder ver por web toda la informacion del Stack
	sudo touch /var/www/html/stack.php
	sudo chmod o+w /var/www/html/stack.php
	STACK=/var/www/html/stack.php
	sudo echo -e "<?php\n	phpinfo();\n	phpinfo(INFO_MODULES);\n?>" > $STACK
	echo ""
	echo "Si desea consultar toda la informacion del stack, visite http://$IP/stack.php"
}

dsoloapache () {
	# Desinstalamos solo apache
	echo "Se desinstalara apache, por favor aguarde"
	sudo apt-get purge -y apache2 apache2-utils apache2-bin apache2.2-common > /dev/null
	echo "Se desinstalo apache"
}

dapachephp () {
	# Desinstalamos apache y php
	echo "Se desinstalara apache y php, por favor aguarde"
	sudo apt-get purge -y apache2 apache2-utils apache2-bin apache2.2-common > /dev/null
	sudo apt-get autoremove -y libapache2-mod-php php > /dev/null
	echo "Se desinstalo apache y php"
}

dapachephpmysql () {
	# Desinstalamos apache php y mysql
	echo "Se desinstalara apache, php y mysql, por favor aguarde"
	sudo apt-get purge -y apache2 apache2-utils apache2-bin apache2.2-common > /dev/null
	sudo apt-get autoremove -y libapache2-mod-php php > /dev/null
	sudo apt-get autoremove -y default-mysql-server > /dev/null
	echo "Se desinstalo apache, php y msql"
}

altasitio () {
	# Pedimos el nombre del sitio
	echo -n "Por favor indique el nombre del nuevo sitio, por ejemplo 'programacion1.local' (sin comillas): "
	read SITIO
	# Creo archivo de Configuracion del sitio
	sudo touch /etc/apache2/sites-available/$SITIO.conf
	SITIODIR=/etc/apache2/sites-available/$SITIO.conf
	# Doy permisos de escritura en el archivo
	sudo chmod o+w $SITIODIR

	# Cargamos la config del archivo default de apache para el sitio nuevo y modifcamos sobre ese template
	sudo cat /etc/apache2/sites-available/000-default.conf | grep -v "#" > $SITIODIR

	# Reemplazamos el Document Root default por el del sitio y especifico ServerName
	sudo sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/$SITIO\n	ServerName $SITIO/g" $SITIODIR

	# Especifico archivo especifico de log del sitio
	sudo sed -i "s/CustomLog \${APACHE_LOG_DIR}\/access.log/CustomLog \${APACHE_LOG_DIR}\/access-$SITIO.log/g" $SITIODIR

	# Creamos el sitio
	sudo mkdir -p /var/www/$SITIO
	sudo chmod o+w /var/www/$SITIO

	# Cargamos un index
	sudo echo "<h1>Tarea Programacion</h1>" > /var/www/$SITIO/index.html

	# Activamos el sitio
	sudo a2ensite $SITIO > /dev/null

	# Cargamos apuntamiento en fichero hosts
	echo ""
	echo "Para simular el apuntamiento DNS, configuraremos el archivo /etc/hosts"
	echo -n "Por favor ingrese la IP del Servidor (x.x.x.x): "
	read IP
	echo ""
	echo -n "Ya tiene un dominio anterior cargado en el fichero para la IP del servidor? (y/n): "
	read YESNO
	# Validamos opcion ingresada
	while [[ -z $YESNO && $YESNO != [yn] ]]
	do
		echo -n "Por favor ingrese un parametro valido (y/n): "
		read YESNO
	done
	# Si ya hay un sitio cargado, lo reemplazamos con sed, si no hay, ponemos la linea al final del fichero
	if [ $YESNO == "y" ]; then
		echo "Esta es la configuracion del fichero hosts en la parte de la IP y Dominio: "
		sudo cat /etc/hosts | grep $IP
		echo ""
		echo -n "Ingrese el nombre del sitio anterior: "
		read OLDSITE
		sudo sed -i "s/$OLDSITE/$SITIO/g" /etc/hosts
	else
		echo "$IP $SITIO" | sudo tee -a /etc/hosts > /dev/null
	fi
	echo ""
	echo "El fichero quedo configurado de la siguiente manera: "
	sudo cat /etc/hosts
	# Reiniciamos el servicio de apache
	sudo systemctl reload apache2

	# Mostramos mensaje final
	echo ""
	echo "El sitio fue activado"
	echo ""
	echo "Testearemos la funcionalidad con wget http://$SITIO/index.html"
	wget http://$SITIO/index.html
}

bajasitio () {
	# Pedimos nombre del sitio a eliminar
	echo -n "Ingrese el nombre del sitio que desea eliminar: "
	read SITEDELETE
	echo "Si desea eliminar el sitio, escriba 'y' sin comillas a medida que se pregunta por los ficheros a eliminar y presione ENTER"
	sudo rm -ri /var/www/$SITEDELETE /etc/apache2/sites-available/$SITEDELETE.conf
       
	# Reiniciamos el servicio
	sudo systemctl reload apache2
	echo ""
	echo "El proceso de borrado del sitio $SITEDELETE finalizo"
}

contarvisitas () {
	echo -n "Ingrese el dominio del sitio que desea consultar: "
	read SITIO
	LOGSITE=/var/log/apache2/access-$SITIO.log
	# Si el fichero existe, cuento y lo muestro. Si no existe, indico que el dominio no existe.
	sudo test -f $LOGSITE
	# Si $? es cero, significa que existe el fichero.
	if [ $? == 0 ]; then
		echo "La cantidad de visitas al sitio son: "; sudo cat /var/log/apache2/access-$SITIO.log | wc -l
	else
		echo "El dominio $SITIO no existe"
	fi
}

iptimestamp () {
	echo -n "Ingrese el dominio del sitio que desea consultar: "
	read SITIO
	LOGSITE=/var/log/apache2/access-$SITIO.log
	# Si el fichero existe, mostramos, sino, indicamos que el dominio no existe.
	sudo test -f $LOGSITE
	# Si $? es cero, existe, entonces mostramos. Sino, indicamos que el dominio no es valido
	if [ $? == 0 ]; then
		echo "Las visitas fueron realizadas por las siguientes IPs en los siguientes horarios: "
		sudo cat /var/log/apache2/access-$SITIO.log | cut -d " " -f1,4
	else
		echo "El dominio $SITIO no es valido"
	fi
}

deploywordpress () {
        # Pedimos el usuario
	echo -n "Por favor ingrese el usuario para la BD: "
        read USER
        # Pedimos la clave
	echo -n "Por favor ingrese la clave para el usuario $USER: "
	read CLAVE
        # Creamos la BD y damos privilegios al usuario
	sudo mysql -e "CREATE DATABASE wordpress" 2>/dev/null
	echo "Se creo la DB wordpress: "
	sudo mysql -e "show databases"
	echo ""
	sudo mysql -e "GRANT ALL ON wordpress.* TO '$USER'@'localhost' IDENTIFIED BY '$CLAVE'"
	echo "Se creo el usuario $USER: "
	echo ""
	sudo mysql -e "SELECT User FROM mysql.user"
	# Flush de privilegios
	sudo mysql -e "FLUSH PRIVILEGES"
	echo "Se creo la base de datos de wordpress con usuario $USER"
	
	# Por tema de permisos, abrimos una consola para poder pararnos en /var/www/html, descargar wordpress y descomrpimirlo
	echo "Se descargara wordpress y se extraera el contenido en /var/www/html/wordpress"
	sudo bash -c "cd /var/www/html; wget https://wordpress.org/latest.tar.gz; tar xf latest.tar.gz"

	# Generamos un fichero de configuracion de php, pegamos la config del fichero de ejemplo y reemplazamos los datos de DB, USER y PWD por
	# los brindados
	sudo cp /var/www/html/wordpress/wp-config-sample.php /var/www/html/wordpress/wp-config-wordpress.php
	
	sudo sed -i "s/define( 'DB_NAME', 'database_name_here' );/define( 'DB_NAME', 'wordpress' );/g" /var/www/html/wordpress/wp-config-wordpress.php
	sudo sed -i "s/define( 'DB_USER', 'username_here' );/define( 'DB_USER', '$USER' );/g" /var/www/html/wordpress/wp-config-wordpress.php
	sudo sed -i "s/define( 'DB_PASSWORD', 'password_here' );/define( 'DB_PASSWORD', '$CLAVE' );/g" /var/www/html/wordpress/wp-config-wordpress.php
	echo "Se finalizo con las configuraciones" 
	echo "Para finalizar el deploy, ingrese a http://$IP/wordpress"
}
