#!/bin/bash
# Declarado el interprete de comandos

# Cambie los permisos rwx de archivos y directorios para los tres tipos: User, Group y Others, segun el conjunto
# de permisos que defina el usuario

#Limpiamos pantalla para comenzar con el script
clear

# Indicamos que el comando se ejecute como root y lo indicamos
echo "Antes de comenzar, informamos que el script debe ser ejecutado como root" 
echo ""
read -p "Si es root presione ENTER, si no es root, CTRL+C e ingrese como root"

# Validamos que efectivamente el usuario sea root
if [ $UID -eq 0 ]; then
	echo "Validando usuario root"
	sleep 1s
	echo -ne ".\r"
	sleep 1s
	echo -ne "..\r"
	sleep 1s
	echo -ne "...\r"
	echo -ne "\n"
	sleep 1s
	read -p "Usuario root validado correctamente, presione ENTER para continuar"
else
	echo "Validando usuario root"
	sleep 1s
	echo -ne ".\r"
	sleep 1s
	echo -ne "..\r"
	sleep 1s
	echo -ne "...\r"
	echo -ne "\n"
	sleep 1s
	read -p "Este usuario no es root, por favor presione ENTER y vuelva a ejecutar como root"
	exit 1
fi
# Primero pedimos el fichero sobre el que aplicaremos el cambio de permisos
# Clareamos pantalla y comenzamos primer punto
clear
echo "Primer funcion: Cambie los permisos rwx de archivos y directorios para los tres tipos: 'User', 'Group' y 'Others', segun"
echo "el conjunto de permisos que defina el usuario"
sleep 2s
echo ""
read -e -p "Ingrese ruta compelta del fichero o directorio que desea cambiar los permisos, luego presione ENTER: " fichero

# Linea en blanco para dejar espacio
echo ""
# Pedimos confirmacion al usuario sobre si esta seguro de lo que ingreso
read -p "Usted ha ingresado: '$fichero' presione Enter para avanzar o Ctrl+C para cancelar" 

# Validamos que el fichero exista, si existe, continuamos, sino indicamos que es invalido
if [ -e "$fichero" ]; then
	echo "Validando si el argumento ingresado existe"
	sleep 1s
	echo -ne ".\r"
	sleep 1s
	echo -ne "..\r"
	sleep 1s
	echo -ne "...\r"
	echo -ne "\n"
	sleep 1s
	read -p "El argumento ingresado se valido correctamente, presione ENTER para avanzar"
else
	echo "Validando si el argumento existe"
	sleep 1s
	echo -ne ".\r"
	sleep 1s 
	echo -ne "..\r"
	sleep 1s
	echo -ne "...\r"
	echo -ne "\n"
	sleep 1s
	read -p "El argumento es invalido, presione ENTER para finalizar e ingresar un fichero valido"
	exit 1
fi


#Creo directorio y variable de error por posibles errores al ingresar argumentos de permisos
mkdir -p /tmp/permisos/
ERRPERM=/tmp/permisos/error.log

# Consultamos los permisos actuales del fichero
echo ""
echo "Los permisos actuales del dato ingresado son: " ; ls -l $fichero | cut -d ' ' -f1
echo ""
# Explicamos metodo de funcionamiento del agregado de permisos para usuarios
echo "Vamos a agregar/remover los permisos de usuarios, grupos y otros ¿Como funciona?"
echo ""
sleep 1s
echo "Primero debemos indircar si el permiso a cambiar es sobre usuarios, grupos u otros ¿Como?"
echo ""
echo "'u' si quiere modificar permisos de usuario"
echo "'g' si quiere modificar permisos de grupos"
echo "'o' si quiere modificar permisos de otros"
echo ""
sleep 2s
echo "Seguido del primer parametro: '+' para que el permiso se agregue o '-' para remover"
echo ""
sleep 1s
echo "Luego especificar el tipo de permiso que quiere modificar" 
echo ""
echo "'r' para modificar permiso de Lectura"
echo "'w' para modificar permiso de Escritura"
echo "'x' para modificar permiso de Ejecucion"
echo ""
sleep 2s
echo "Una vez que tiene la primer combinacion, puede repetir (separado de un espacio) las combinaciones que desee"
echo ""
sleep 1s
echo "Ejemplo: Agregar permisos de escritura a usuarios y lectura a grupos: u+w g+r"
echo ""
read -p "NOTA: Ingresar un parametro erroneo o nulo, mantiene el permiso. Presione ENTER para avanzar e ingresar los permisos"
echo ""
read -p "Ingrese los permisos que desea agregar: " permiso1 permiso2 permiso3 permiso4 permiso5 permiso6 permiso7 permiso8 permiso9

# Agregamos los permisos con los parametros que ingresa el usuario, si hay errores los mandamos a /tmp/permisos/error.log
chmod $permiso1 $fichero 2>> $ERRPERM
chmod $permiso2 $fichero 2>> $ERRPERM
chmod $permiso3 $fichero 2>> $ERRPERM
chmod $permiso4 $fichero 2>> $ERRPERM
chmod $permiso5 $fichero 2>> $ERRPERM
chmod $permiso6 $fichero 2>> $ERRPERM
chmod $permiso7 $fichero 2>> $ERRPERM
chmod $permiso8 $fichero 2>> $ERRPERM
chmod $permiso9 $fichero 2>> $ERRPERM

# Texto que mostramos en pantalla durante la ejecucion del script
echo "Modificando permisos"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s
echo "Permisos de usuarios modificados"

# Fin de este punto
echo "Operaciones finalizada, los permisos sobre $fichero quedaron de la siguiente manera: " ; ls -l $fichero | cut -d ' ' -f1
echo ""
echo "En caso de querer verificar un error sobre la operacion, consultar el archivo /tmp/permisos/error.log"
echo ""
read -p "El primer punto finalizo, presione ENTER para avanzar al siguiente o punto o Ctrl+C para cancelar"

# Limpiamos la pantalla para el punto que sigue
clear
# Consigna
echo "Funcion: Permita cambiar el dueno y grupo de ficheros y directorios definidos por el usuario."
echo ""
# Creo directorio y fichero donde almanamos logs de error.

mkdir -p /tmp/grupos/
GRPERR=/tmp/grupos/error.log

# Muestro metodo de uso en pantalla
echo "Metodo de uso: Se va a solicitar el fichero o directorio sobre el que desea trabajar"
echo "luego debera ingresar el nuevo propietario y si tambien desea el nuevo grupo, solo quiere cambiar el dueno, deje el grupo vacio"
echo ""
read -e -p "Ingrese la ruta del directorio o fichero que desea modificar (/path/del/directorio/fichero): " argumento
echo ""
# Validamos existencia del argumento
if [ -e "$argumento" ]; then
        echo "Validando si el argumento ingresado existe"
        sleep 1s
        echo -ne ".\r"
        sleep 1s
        echo -ne "..\r"
        sleep 1s
        echo -ne "...\r"
        echo -ne "\n"
        sleep 1s
	read -p "El argumento es valido, presione ENTER para continuar"
else
        echo "Validando si el argumento existe"
        sleep 1s
        echo -ne ".\r"
        sleep 1s
        echo -ne "..\r"
        sleep 1s
        echo -ne "...\r"
        echo -ne "\n"
        sleep 1s
        read -p "El argumento es invalido, presione ENTER para finalizar e ingresar un fichero valido"
        exit
fi

# Pedimos el nuevo propietario
read -p "Ingrese el nuevo propietario y presione ENTER: " nuevopropietario
echo ""
# Pedimos el nuevo grupo
read -p "Ingrese el nuevo grupo y presione ENTER (opcional): " nuevogrupo
echo ""
# Consultamos si tambien se quieren cambiar subcarpetas y ficheros del directorio padre
read -p "Desea tambien modificar subcarpetas y ficheros? (si/no): " sino
# Si la puerta es si, chown con opcion -R
if [[ $sino == "si" ]]; then
        echo "Se aplicaran cambios modificando subdirectorios y archivos"
        sudo chown -R $nuevopropietario:$nuevogrupo $argumento 2>> $GRPERR
else
	# Si la respuesta es no, cambiar sin opcion -R
        echo "Se aplicaran cambios sin modificar subdirectorios y archivos"
        sudo chown $nuevopropietario:$nuevogrupo $argumento 2>> $GRPERR
fi

# Muestro en pantalla que se estan haciendo los cambios
echo "Aplicando cambios"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s
echo "El cambio fue aplicado, si quiere chequear por errores consultar el archivo /tmp/grupos/error.log"

read -p "Presione ENTER para avanzar al siguiente punto o Ctrl+C para salir"
# Fin consigna 2

# Inicio consigna 3
# Consigna: Permita crear y eliminar grupos definidos por el usuario:

# Creo directorio para errores
mkdir -p /tmp/consigna3/
PUNTO3=/tmp/consigna3/error.log

# Limpio pantalla
clear

# Indico lo que va a realizar la consigna
echo "Funcion: Crear y eliminar grupos definidos por el usuario"
sleep 1s
echo ""
echo "Metodo de uso: Se va a consultar si desea crear o eliminar un grupo, luego de seleccionar indicar grupo que desea modificar."
echo ""
read -p "¿Quiere eliminar o crear un grupo? (crear/eliminar): " crearborrargrupo

# En base a lo elegido por el usuario avanzamos

if [[ $crearborrargrupo == "crear" ]]; then
	# Pedimos nombre del grupo que se va a crear
	read -p "Ingrese nombre del nuevo grupo: " newgroup
	groupadd $newgroup 2>> $PUNTO3
	echo "Creando grupo $newgroup"
        sleep 1s
        echo -ne ".\r"
        sleep 1s
        echo -ne "..\r"
        sleep 1s
        echo -ne "...\r"
        echo -ne "\n"
        sleep 1s
        echo "El grupo fue creado, si quiere chequear por errores consultar el archivo /tmp/consigna3/error.log"
elif [[ $crearborrargrupo == "eliminar" ]]; then
	# Pedimos nombre de grupo a borrar
        read -p "Ingrese nombre del grupo a borrar: " delgroup
        groupdel $delgroup 2>> $PUNTO3
        echo "Eliminando grupo $delgroup"
        sleep 1s
        echo -ne ".\r"
        sleep 1s
        echo -ne "..\r"
        sleep 1s
        echo -ne "...\r"
        echo -ne "\n"
        sleep 1s
        echo "El grupo fue eliminado, si quiere chequear por errores consultar el archivo /tmp/consigna3/error.log"
else
	# No ingresaron la opcion correctamente
	echo "No escribio 'crear' o 'eliminar' correctamente"
	exit 1
fi

read -p "Consigna 3 finalizada, presione ENTER para avanzar o Ctrl+C para salir"

# Consigna 4: Listar grupos existentes
# Limpiamos pantalla
clear
# Indicamos que hace esta consigna
echo "Funcion: Listar los grupos existentes"
read -p "¿Desea listar los grupos? (si/no)?: " consigna4

if [[ $consigna4 == "si" ]]; then
        cat /etc/group | cut -d : -f1
elif [[ $consigna4 == "no" ]]; then
        read -p "No seran listados los grupos, presione Enter para avanzar"
else
        echo "Ingreso un valor erroneo, por favor vuelva a ejecutar el script"
        exit 1
fi
read -p "Fin de la Consigna 4, presione ENTER para continuar o Ctrl+C"

# Consigna 5: Buscar ficheros y directorios segun un conjunto de permisos definidos por el usuario
# Limpiamos pantalla
clear

# Muestro consigna en pantalla
echo "Consigna 5: Buscar fichero y directorios segun un conjunto de permisos definidos por el usuario"
echo "¿Como funciona el script?"
echo ""
# Mostramos y listamos las posibles opciones de permisos a pedir
echo "Tenemos las siguientes opciones de permisos a consultar: "
echo "0 = ninguno"
echo "1 = ejecucion"
echo "2 = escritura"
echo "3 = escritura y ejecucion"
echo "4 = lectura"
echo "5 = lectura y ejecucion"
echo "6 = lectura y escritura"
echo "7 = lectura, escritura y ejecucion"
echo ""
sleep 3s
# Pido el path del directorio sobre el cual voy a buscar los archivos
read -e -p "Ingrese el path del directorio en el que desea realizar la busqueda: (/path/del/directorio): " pathdeldirectorio
sleep 1s
# Pido si quiero buscar permisos por usuario, grupos y otros
read -p "Desea buscar permisos por usuarios? grupos? otros?(usuario/grupo/otros): " ugo20

# Busqueda de permisos por usuario
if [[ $ugo20 == "usuario" ]]; then
	# Pido las opciones que explicamos al principio
        read -p "Ingrese la opcion de permisos que desea buscar (0 a 7): " numpermiso
	# Muestro los ficheros en base a la opcion que elige el usuario
        if [[ $numpermiso == 0 ]]; then
                echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /000
        elif [[ $numpermiso == 1 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /001
        elif [[ $numpermiso == 2 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /010
        elif [[ $numpermiso == 3 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /011
        elif [[ $numpermiso == 4 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /100
        elif [[ $numpermiso == 5 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /101
        elif [[ $numpermiso == 6 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /110
        else
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /111
        fi
# Busqueda de permisos por grupos
elif [[ $ugo20 == "grupo" ]]; then
	# Pido las opciones que explicamos al principio
        read -p "Ingrese la opcion de permisos que desea buscar (0 a 7): " numpermiso1
	# Muestro los ficheros en base a la opcion que elige el usuario
        if [[ $numpermiso1 == 0 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /000
        elif [[ $numpermiso1 == 1 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /001
        elif [[ $numpermiso1 == 2 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /010
        elif [[ $numpermiso1 == 3 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /011
        elif [[ $numpermiso1 == 4 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /100
        elif [[ $numpermiso1 == 5 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /101
        elif [[ $numpermiso1 == 6 ]]; then
                echo "Los ficheros/directorios son: " ;find $pathdeldirectorio -perm /110
        else
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /111
        fi
# Busqueda de permisos por grupos
elif [[ $ugo20 == "otros" ]]; then
	# Pido las ocpiones que explicamos al principio
        read -p "Ingrese la opcion de permisos que desea buscar (0 a 7): " numpermiso3
	# Muestro los ficheros en base a la opcion que elige el usuario
        if [[ $numpermiso3 == 0 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /000
        elif [[ $numpermiso3 == 1 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /001
        elif [[ $numpermiso3 == 2 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /010
        elif [[ $numpermiso3 == 3 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /011
        elif [[ $numpermiso3 == 4 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /100
        elif [[ $numpermiso3 == 5 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /101
        elif [[ $numpermiso3 == 6 ]]; then
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /110
        else
               echo "Los ficheros/directorios son: " ; find $pathdeldirectorio -perm /111
        fi
else
        echo "Ingreso una opcion incorrecta, vuelva a ejecutar el script por favor"
        exit 2
fi
read -p "La consigna 5 finalizo, presione ENTER para avanzar o Ctrl+C para salir"

# Ultima consigna

# Doy permisos para ejecutar el script sin escribir bash delante
chmod u+x $0
# Limpio pantalla
clear
# Consigna 6: Segun como termine el script, definir el exit status del programa
# Muestro consigna en pantalla
echo "Definimos el exit status del programa en base a como termine"
sleep 1s
echo "Definiendo el status final"
sleep 1s
echo -ne ".\r"
sleep 1s
echo -ne "..\r"
sleep 1s
echo -ne "...\r"
echo -ne "\n"
sleep 1s
if [ $? == 0 ]; then
        echo "El programa finalizo exitosamente"
else
        echo "El programa finalizo con con errores"
fi
